/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    
    void audioSetAmplitude(float value);
    float audioGetAmplitude();
    void audioSetMetroAmplitude(float value);
    float audioGetMetroAmplitude();
    
    int audioGetSampleRate() const;
    
    void beep();
private:
    AudioDeviceManager audioDeviceManager;
    CriticalSection sharedMemory;
    
    SinOscillator sinOscillator;
    SinOscillator beepOscillator;
    
    float amplitude;
    float metroAmplitude;
    float sampleRate;
    const float twoPi = 2 * M_PI;
    
    float synthOutLeft;
    float synthOutRight;
    float mix;
    float metro;
};



#endif  // AUDIO_H_INCLUDED
