/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    // Set some defaults for the synth.
    sharedMemory.enter();
    sinOscillator.setFrequency(440.f);
    sinOscillator.setAmplitude(0.5f);
    sharedMemory.exit();
    
    synthOutLeft = 0.f;
    synthOutRight = 0.f;
    mix = 0.f;
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    
    if (message.isNoteOnOrOff())
    {
        sharedMemory.enter();
        sinOscillator.setFrequency(MidiMessage::getMidiNoteInHertz(message.getNoteNumber()));
        sharedMemory.exit();
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    // Work out the phase increment, does not change rapidly so does not need to be in the while loop.
    sinOscillator.setPhaseIncrement((twoPi * sinOscillator.getFrequency()) / sampleRate);
    
    // Set the amplitude.
    sharedMemory.enter();
    sinOscillator.setAmplitude(amplitude);
    sharedMemory.exit();
    
    while(numSamples--)
    {
        // Get the next sample!
        mix = sinOscillator.getSample();
        metro = beepOscillator.getSample();
        
        synthOutLeft = synthOutRight = mix;
        
        *outL = metro + mix;
        *outR = metro + mix;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate = device->getCurrentSampleRate();
}

void Audio::audioDeviceStopped()
{

}

void Audio::audioSetAmplitude(float value)
{
    sharedMemory.enter();
    amplitude = value;
    sharedMemory.exit();
}

float Audio::audioGetAmplitude()
{
    sharedMemory.enter();
    return amplitude;
    sharedMemory.exit();
}

void Audio::audioSetMetroAmplitude(float value)
{
    sharedMemory.enter();
    metroAmplitude = value;
    sharedMemory.exit();
}

float Audio::audioGetMetroAmplitude()
{
    sharedMemory.enter();
    return metroAmplitude;
    sharedMemory.exit();
}

int Audio::audioGetSampleRate() const
{
    return sampleRate;
}

void Audio::beep()
{
    uint32 time = Time::getMillisecondCounter();
    
    beepOscillator.setFrequency(440.f);
    beepOscillator.setAmplitude(0.5f * metroAmplitude);
    beepOscillator.setPhaseIncrement((twoPi * beepOscillator.getFrequency()) / sampleRate);
    
    Time::waitForMillisecondCounter (time + 100);
    beepOscillator.setAmplitude(0.f);
}