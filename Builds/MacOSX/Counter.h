//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/17/14.
//
//

#ifndef COUNTER_H
#define COUNTER_H

#include "../../JuceLibraryCode/JuceHeader.h"
#include <iostream>

class Counter : public Thread
{
public:
    Counter();
    ~Counter();
    
    void startCounter();
    void stopCounter();
    void resetCounter();
    void setPauseTime(uint32 time);
    
    bool isCounterRunning();
    
    void run() override;
    
    /** Class for counter listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    void setListener (Listener* newListener);
    
private:
    uint32 counter;
    uint16 pause;
    Listener* listener;
};

#endif /* defined(COUNTER_H) */
