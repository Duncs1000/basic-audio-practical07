//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/17/14.
//
//

#include "SinOscillator.h"

SinOscillator::SinOscillator ()
{
    frequency = 440.f;
    phasePosition = 0.f;
}

SinOscillator::~SinOscillator()
{
    
}

// Where the actual wave is calculated...
float SinOscillator::getSample()
{
    phasePosition += phaseIncrement;
    
    if (phasePosition > twoPi)
        phasePosition -= twoPi;
    
    float sample = sin(phasePosition);
    
    return sample * amplitude;
}

void SinOscillator::setFrequency(float value)
{
    frequency = value;
}

float SinOscillator::getFrequency() const
{
    return frequency;
}

void SinOscillator::setAmplitude(float value)
{
    amplitude = value;
}

float SinOscillator::getAmplitude() const
{
    return amplitude;
}

void SinOscillator::setPhaseIncrement(float value)
{
    phaseIncrement = value;
}