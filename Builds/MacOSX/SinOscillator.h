//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/17/14.
//
//

#ifndef SINOSCILLATOR_H
#define SINOSCILLATOR_H

/**
 Sine Oscillator Class
 */

#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator
{
public:
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    float getSample();
    
    void setFrequency(float value);
    float getFrequency() const;
    
    void setAmplitude(float value);
    float getAmplitude() const;
    
    void setPhaseIncrement(float value);
    
private:
    float frequency;
    float amplitude;
    float phasePosition;
    float phaseIncrement;
    const float twoPi = 2 * M_PI;
};

#endif /* defined(SINOSCILLATOR_H) */
